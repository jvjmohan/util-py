import os
import time
import csv

csv_output_file = open("File_stats_work1.csv","w",newline='')
keys = ["file_location", "file_name", "size", "modified_dt","create_dt"]
csv_writer = csv.DictWriter(csv_output_file,keys)


def get_file_metadata(path, file_name):
    file_metadata = {}
    file_location = os.path.join(path, file_name)
    size = os.path.getsize(file_location)
    modified_dt = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.path.getmtime(file_location)))
    create_dt = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.path.getctime(file_location)))
    file_metadata.update(
        {"file_location": path, "file_name": file_name, "size": size, "modified_dt": modified_dt,
         "create_dt": create_dt})

    return file_metadata


def write_to_csv(files_in_folder):
    csv_writer.writerows(files_in_folder)


def scan(i_folder):
    for path, dir, files in os.walk(i_folder):
        files_in_folder = []
        for file in files:
            _file_metadata = get_file_metadata(path, file)
            files_in_folder.append(_file_metadata)
        if len(files_in_folder) > 0:
            write_to_csv(files_in_folder)


scan(r"C:\Users\")
